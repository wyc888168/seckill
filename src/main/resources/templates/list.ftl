<!DOCTYPE html>
<html lang="en">
<head>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <meta charset="UTF-8">
    <title>秒杀商品列表</title>
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <h2 class="text-center">
                秒杀列表
            </h2>
            <table class="table">
                <thead>
                <tr>
                    <th>名称</th>
                    <th>库存</th>
                    <th>开始时间</th>
                    <th>结束时间</th>
                    <th>详情页</th>
                </tr>
                </thead>
                <tbody>
                <#list list as sk>
                <tr>
                    <td>${sk.name}</td>
                    <td>${sk.number}</td>
                    <td>
                    ${sk.startTime?datetime}
                    </td>
                    <td>
                    ${sk.endTime?datetime}
                    </td>
                    <td><a class="btn btn-info" href="/seckill/${sk.seckillId}/detail" target="_blank">详情</a></td>
                </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
</html>