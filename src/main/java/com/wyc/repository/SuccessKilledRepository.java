package com.wyc.repository;

import com.wyc.dataobject.SuccessKilled;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by weiyongchang
 * Description:
 * Date: 2017/10/31
 */
public interface SuccessKilledRepository extends JpaRepository<SuccessKilled, Long> {

}
