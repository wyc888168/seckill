package com.wyc.dto;


import lombok.Data;

import java.io.Serializable;

//将所有的ajax请求返回类型，全部封装成json数据
@Data
public class SeckillResult<T> implements Serializable{

    private static final long serialVersionUID = 9175918100467348784L;

    //请求是否成功
    private boolean success;
    private T data;
    private String error;

    public SeckillResult(boolean success, T data) {
        this.success = success;
        this.data = data;
    }

    public SeckillResult(boolean success, String error) {
        this.success = success;
        this.error = error;
    }

    public boolean isSuccess() {
        return success;
    }

}
