package com.wyc.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by weiyongchang
 * Description:
 * Date: 2017/10/31
 */

@Data
public class SeckillDetail {

    private long seckillId;

    private String userPhone;

    private String name;

    private BigDecimal price;

    private int state;

    private Date startTime;

    private Date endTime;

    @Override
    public String toString() {
        return "SeckillDetail{" +
                "seckillId=" + seckillId +
                ", userPhone='" + userPhone + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", state=" + state +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
