package com.wyc.dataobject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by weiyongchang
 * Description:
 * Date: 2017/10/31
 */
@Entity
@Data
@DynamicUpdate
@AllArgsConstructor
@NoArgsConstructor
public class Seckill implements Serializable{

    private static final long serialVersionUID = 8798356009742656039L;

    @Id
    private long seckillId;

    private String name;

    private int number;

    private BigDecimal price;

    private Date createTime;

    private Date endTime;

    private Date startTime;

    @Override
    public String toString() {
        return "Seckill{" +
                "seckillId=" + seckillId +
                ", name='" + name + '\'' +
                ", number=" + number +
                ", price=" + price +
                ", createTime=" + createTime +
                ", endTime=" + endTime +
                ", startTime=" + startTime +
                '}';
    }
}
