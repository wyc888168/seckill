package com.wyc.dataobject;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * Created by weiyongchang
 * Description:
 * Date: 2017/10/31
 */
@Data
@Entity
@DynamicUpdate
public class SuccessKilled {

    @Id
    private long seckillId;

    @NotBlank(message = "手机号不能为空")
    @Length(min = 11, max = 11, message = "手机号有误")
    @Pattern(regexp = "^(\\([0-9]+\\)|\\+?[0-9]+)-?[0-9]+$")
    private String userPhone;

    private int status;

    private Date createTime;

    @Override
    public String toString() {
        return "SuccessKilled{" +
                "seckillId=" + seckillId +
                ", userPhone='" + userPhone + '\'' +
                ", status=" + status +
                ", createTime=" + createTime +
                '}';
    }
}
